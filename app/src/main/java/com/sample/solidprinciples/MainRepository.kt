package com.sample.solidprinciples

import javax.inject.Inject

class MainRepository @Inject constructor(
    private val auth: Authenticator,
    private val fileLogger: FileLogger
) {

    suspend  fun loginUser(email: String, password: String) {
        try {
            auth.signInWithEmailAndPassword(email, password)
        } catch (e: Exception) {
            fileLogger.logError(e.message.toString())
        }
    }

}