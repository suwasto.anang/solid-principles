package com.sample.solidprinciples

import com.google.firebase.auth.FirebaseAuth

interface Authenticator {

    suspend fun signInWithEmailAndPassword(email: String, password: String)

}

class FirebaseAuthenticator: Authenticator {

    override suspend fun signInWithEmailAndPassword(email: String, password: String) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
    }
}

class CustomApiAuthenticator: Authenticator {

    override suspend fun signInWithEmailAndPassword(email: String, password: String) {
        TODO("Not yet implemented")
    }
}